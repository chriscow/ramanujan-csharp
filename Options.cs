using CommandLine;

namespace Raman
{
    public class Options
    {
        [Option('r', "rebuild", Required = false, HelpText = "Force rebuild of all hashtables.")]
        public bool Rebuild { get; set; }
    }
}