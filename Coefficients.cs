using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using IniParser.Model;

namespace Raman
{
    public class Coefficients
    {
        int _terms;
        double[] _digits;

        string _str;

        public int Count { get { return (int)Math.Pow(_digits.Length, _terms); } }

        public Coefficients(int terms, IEnumerable<int> digits)
        {
            _terms = terms;
            _digits = digits.Select(i => (double)i).ToArray();

            _str = $"t{terms} {string.Join(",", digits.Take(3))}";
        }

        public Coefficients(int terms, IEnumerable<double> digits)
        {
            _terms = terms;
            _digits = digits.ToArray();
            _str = $"t{terms} {string.Join(",", digits.Take(3))}";
        }

        public Coefficients(int terms, string config)
        {
            bool isRange = false;
            bool isSet = false;

            _str = $"t{terms} {config}";


            //
            // A range has square brackets and a set has curly brackets.
            //
            if (config.StartsWith('[') && config.EndsWith(']') && config.Contains(','))
                isRange = true;
            else if (config.StartsWith('{') && config.EndsWith('}') && config.Contains(','))
                isSet = true;
            else
                throw new ArgumentException($"Unable to parse '{config}' into coefficients");

            _terms = terms;
            config = config.Substring(1, config.Length - 2);
            var tokens = config.Split(',');

            if (isRange)
            {
                if (tokens.Length != 2)
                    throw new ArgumentException($"Unable to parse range: {config}");

                var startVal = int.Parse(tokens[0]);
                var endVal = int.Parse(tokens[1]);

                _digits = Enumerable.Range(startVal, endVal - startVal + 1).Select(i => (double)i).ToArray();
            }
            else if (isSet)
            {
                _digits = tokens.Select(i => double.Parse(i)).ToArray();
            }
        }

        public override string ToString()
        {
            return _str;
        }

        public List<double[]> All()
        {
            int count = this.Next().Count();
            var result = new List<double[]>();

            foreach (var coeff in this.Next())
            {
                result.Add(coeff);
            }

            return result;
        }

        public IEnumerable<double[]> Next()
        {
            bool carry = false;
            bool overflow = false;

            // Each coefficient in the polynomial uses a set of digits to 'count' through
            // digits:  1, 3, 5, 7
            // terms: 2
            // sequence will be:
            // 1,1 => 3,1 => 5,1 => 7,1 => 1,3 => 3,3 => 5,3 ... 5,7, 7,7

            // This keeps the index of the digit we are on
            int[] digit = new int[_terms];

            while (true)
            {
                double[] coefficients = new double[_terms];
                for (int term = 0; term < _terms; term++)
                {
                    // _aDigits is the set of all coefficient values (-1,0,1,2,3 ...)
                    //                                                     ^
                    // digit[i] is the index of which digit value we are currently on
                    //
                    coefficients[term] = _digits[digit[term]];

                    // Stop when we have gone through all possible combinations
                    if (overflow && term + 1 == _terms)
                        yield break;

                    if (0 == term || carry)
                    {
                        digit[term]++;
                        carry = false;
                    }

                    if (digit[term] == _digits.Length)
                    {
                        // If we are at the end of the set of digits, wrap it around
                        // by setting the carry flag. This will move the next most 
                        // significant digit to the next value.

                        digit[term] = 0;
                        carry = true;

                        if (term + 1 == _terms)
                        {
                            overflow = true;
                        }
                    }
                }

                yield return coefficients;
            }
        }
    }
}