﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.IO;

using CommandLine;

using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;

using IniParser;
using IniParser.Model;

using MessagePack;

using AlgoResult = System.Tuple<object,double>;


namespace Raman
{
    class Program
    {
        static IniData _config;

        static void Main(string[] args)
        {
            bool rebuild = false;

            Parser.Default.ParseArguments<Options>(args)
            .WithParsed<Options>(o =>
            {
                if (o.Rebuild)
                {
                    rebuild = true;
                }
            });

            loadConfig();

            var progressBar = new ProgressBar();

            //
            // Get the algorithms to use for the left and right hand sides.
            //
            var algoType = _config["LHS"]["algorithm"].ToAlgoType();
            var lhsAlgo = AlgorithmFactory.Create(algoType, _config);

            algoType = _config["RHS"]["algorithm"].ToAlgoType();
            var rhsAlgo = AlgorithmFactory.Create(algoType, _config);

            //
            // Number of decimal places the hash function uses
            //
            int hashPrecision = int.Parse(_config["Program"]["hash_precision"]);
            
            var rhsHashes = HashTable.Load(rhsAlgo, hashPrecision);
            if (rebuild || 0 == rhsHashes.Buckets.Count)
            {
                rhsHashes.Progress += progressBar.UpdateProgress;
                progressBar.Start();

                rhsHashes.Build();
                rhsHashes.Save();

                progressBar.Stop();
            }

            progressBar = new ProgressBar();
            var lhsHashes = HashTable.Load(lhsAlgo, hashPrecision);
            if (rebuild || 0 == lhsHashes.Buckets.Count)
            {
                progressBar.Start();
                lhsHashes.Progress += progressBar.UpdateProgress;

                lhsHashes.Build();
                lhsHashes.Save();

                progressBar.Stop();
            }

            var matches = new List<int>();
            foreach (var key in lhsHashes.Buckets)
            {
                if (rhsHashes.ContainsKey(key))
                {
                    matches.Add(key);
                }
            }

            var hash = rhsHashes.GetHash(Math.E);

            Console.WriteLine($"hash:{hash} lhs:{lhsHashes.ContainsKey(hash)} rhs:{rhsHashes.ContainsKey(hash)}");

            
        }

        static void loadConfig()
        {
            var iniParser = new FileIniDataParser();
            _config = iniParser.ReadFile("config.ini");
        }
    }
}