using System;
using System.Threading;
using System.Threading.Tasks;

namespace Raman
{
    public class ProgressBar
    {
        bool _canControlCursor = true;

        long _completed = 0;
        long _totalWork = 100;

        bool _continue = true;

        Task _progressTask;

        public ProgressBar()
        {
            try
            {
                _canControlCursor = true;
                Console.CursorLeft = 0;
            }
            catch
            {
                _canControlCursor = false;
            }
        }

        public Task Start()
        {
            _completed = 0;
            _totalWork = 100;

            Console.WriteLine("                                                                                 ");

            //
            // Separate thread to draw the progress bar
            //
            _progressTask = Task.Run(() => 
            {
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                var elapsedSec = 0;
                TimeSpan eta = new TimeSpan();

                while (_completed < _totalWork && _continue) 
                {
                    // update the progress bar every second
                    drawProgressBar(eta);
                    Thread.Sleep(1000);      
                    elapsedSec++;   

                    // How many items got done over the last second?
                    var progressPerSec = _completed / elapsedSec;

                    var remainSec = long.MaxValue;
                    if (progressPerSec > 0)
                    {
                        remainSec = (_totalWork - _completed) / progressPerSec;
                    }
                    
                    // Just in case remainSec exceeds an int's maximum value, clamp it
                    remainSec = remainSec > int.MaxValue ? int.MaxValue : remainSec;
                    
                    eta = new TimeSpan(0, 0, (int)remainSec);
                } 

                // Just to be sure the progress bar shows 100% at the end
                _completed = _totalWork;
                sw.Stop();
                drawProgressBar(sw.Elapsed);
            });

            return _progressTask;
        }

        public void Stop()
        {
            _continue = false;
            _progressTask.Wait();
            Console.WriteLine("                                                                                 ");
        }

        public void UpdateProgress(long current, long total)
        {
            Interlocked.Exchange(ref _completed, current);
            Interlocked.Exchange(ref _totalWork, total);
        }

        private void drawProgressBar(TimeSpan eta)
        {
            float pct = (float)_completed / _totalWork * 100f;

            if (!_canControlCursor)
            {
                Console.WriteLine($"{_completed} / {_totalWork}  {Math.Truncate(pct)}%  {eta}         ");
            }
            else
            {
                //draw empty progress bar
                Console.CursorLeft = 0;
                Console.Write("["); //start
                Console.CursorLeft = 32;
                Console.Write("]"); //end
                Console.CursorLeft = 1;
                float onechunk = 30.0f / _totalWork;

                //draw filled part
                int position = 1;
                for (int i = 0; i < onechunk * _completed; i++)
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.CursorLeft = position++;
                    Console.Write(" ");
                }

                // var str = _completed.ToString();
                // Console.BackgroundColor = ConsoleColor.Black;
                // Console.Write(str);
                // position += str.Length;

                //draw unfilled part
                for (int i = position; i <= 31 /*- str.Length*/; i++)
                {
                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.CursorLeft = position++;
                    Console.Write(" ");
                }

                //draw totals
                Console.CursorLeft = 33;
                Console.BackgroundColor = ConsoleColor.Black;
                
                Console.Write($"  {Math.Truncate(pct)}%  {eta}         "); //blanks at the end remove any excess
            }
        }
    }
}