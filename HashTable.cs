using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using MessagePack;
using MessagePack.ImmutableCollection;

using Newtonsoft.Json;

using AlgoResult = System.Tuple<object, double>;
using AlgoResultList = System.Collections.Generic.List<System.Tuple<object, double>>;

namespace Raman
{
    public class HashTable
    {
        const string HASHTABLE_PATH = "./Hashtables/";

        /// <summary>
        /// Dictionary whos key is the hash of the algorithm result to 'Precision'
        /// decimal places. The value is a list of algorithm argument/result pairs (Tuple).
        /// We are using a list because some algorithm results may resolve to the same
        /// hash value.
        /// </summary>
        /// <returns></returns>
        ConcurrentDictionary<int, AlgoResultList> _data = new ConcurrentDictionary<int, AlgoResultList>();

        public string Version;

        public AlgorithmType Type;

        public ICollection<int> Buckets { get { return _data.Keys; } }

        public AlgoResult[] this[int hash]
        {
            get
            {
                if (_data.ContainsKey(hash))
                {
                    return _data[hash].ToArray();
                }

                return null;
            }
        }

        /// <summary>
        /// Decimal places of the algorithm result used for hash value
        /// </summary>
        public int Precision;

        /// <summary>
        /// Filename for the actual binary hashed values. The file is in 
        /// MessagePack format.
        /// </summary>
        public string Filename;


        [JsonIgnore]
        /// <summary>
        /// Concrete algorithm implementation.
        /// </summary>
        /// <value></value>
        public IAlgorithm Algorithm { get; private set; }

        /// <summary>
        /// The progress event is fired to allow clients to know how far along
        /// we are in generating the hashtable.
        /// </summary>
        public event Action<long, long> Progress;

        public HashTable()
        {

        }


        public HashTable(IAlgorithm algorithm, int precision)
        {
            Version = "v1.0.0";
            Algorithm = algorithm;
            Type = algorithm.Type;
            Precision = precision;

            Algorithm.Progress += (complete, total) =>
            {
                if (Progress != null)
                    Progress(complete, total);
            };

            Algorithm.Result += addResult;
        }


        public int GetHash(double result)
        {
            return result.Truncate(Precision).GetHashCode();
        }

        public bool ContainsKey(int key)
        {
            return _data.ContainsKey(key);
        }

        public void Build()
        {
            // Used for the progress bar
            var total = Algorithm.Count;

            //
            // Start a thread for each set of a-coefficients. We don't need
            // a thread for the b's cause we don't have enough work or threads.
            //
            var cpus = Environment.ProcessorCount;
            Console.WriteLine($"Building {Algorithm.ToString()} hashtable using {cpus} threads...");

            Algorithm.Result += addResult;
            
            Algorithm.Compute();
        }

        public static HashTable Load(IAlgorithm algorithm, int precision)
        {
            HashTable result;
            var filename = algorithm.ToString();

            var path = Path.Combine(HASHTABLE_PATH, filename) + ".json";

            if (!File.Exists(path))
            {
                Console.WriteLine(Directory.GetCurrentDirectory());
                result = new HashTable(algorithm, precision);
            }
            else
            {
                var json = File.ReadAllText(path);
                result = JsonConvert.DeserializeObject<HashTable>(json);

                var bytes = File.ReadAllBytes(result.Filename);
                result._data = MessagePackSerializer.Deserialize<ConcurrentDictionary<int, AlgoResultList>>(bytes);
            }

            result.Algorithm = algorithm;
            return result;
        }


        public void Save()
        {
            if (!Directory.Exists(HASHTABLE_PATH))
                Directory.CreateDirectory(HASHTABLE_PATH);

            var path = Path.Combine(HASHTABLE_PATH, Algorithm.ToString());
            Filename = path + ".hashtable";

            var json = JsonConvert.SerializeObject(this, Formatting.Indented);
            File.WriteAllText(path + ".json", json);

            var bytes = MessagePackSerializer.Serialize<ConcurrentDictionary<int, AlgoResultList>>(this._data);
            File.WriteAllBytes(Filename, bytes);
        }

        /// <summary>
        /// Adds an algorithm result to the hashtable.
        /// </summary>
        /// <param name="result">2-element Tuple where Item1 is the algorithm arguments and Item2 is the algorithm result</param>
        void addResult(AlgoResult result)
        {
            var hash = GetHash(result.Item2);

            if (!_data.ContainsKey(hash))
                _data[hash] = new AlgoResultList();

            _data[hash].Add(result);
        }
    }
}