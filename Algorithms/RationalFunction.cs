using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using IniParser.Model;

using AlgoResult = System.Tuple<object, double>;

namespace Raman
{
    public class RationalFunction : IAlgorithm
    {
        double _constant;

        Coefficients _aCoeff;
        Coefficients _bCoeff;

        public event Action<long, long> Progress;
        public event Action<AlgoResult> Result;


        public AlgorithmType Type { get { return AlgorithmType.RationalFunction; } }

        public int Count { get; private set; }
        public RationalFunction(IniData iniFile)
        {
            var config = iniFile[GetType().Name];

            var a_degrees = int.Parse(config["a_degrees"]);
            var b_degrees = int.Parse(config["b_degrees"]);

            _aCoeff = new Coefficients(a_degrees, config["a_coeffs"]);
            _bCoeff = new Coefficients(b_degrees, config["b_coeffs"]);

            _constant = double.Parse(config["constant"]);

            Count = _aCoeff.Count * _bCoeff.Count;
        }


        public void Compute()
        {
            int total = _aCoeff.Count;
            int aIndex = -1;

            Parallel.ForEach(_aCoeff.Next(), (aCoeff, state, index) =>
            {
                aIndex++;
                var bIndex = 0;

                if (Progress != null)
                    Progress(aIndex, total);

                foreach (var bCoeff in _bCoeff.Next())
                {
                    bIndex++;

                    var aResult = aCoeff.EvalPolyAt(_constant);
                    var bResult = bCoeff.EvalPolyAt(_constant);

                    var result = aResult / bResult;
                    // Console.WriteLine(result + " = " + aCoeff.ToPolyString(_constant) + "/" + bCoeff.ToPolyString(_constant));

                    if (result.IsApproximately(Math.E))
                    {
                        Console.WriteLine($"{result}");
                    }

                    if (Result != null)
                        Result(new AlgoResult(new { a = aIndex, b = bIndex }, result));
                }
            });
        }

        public override string ToString()
        {
            return GetType().Name;
        }
    }
}