using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

using IniParser;
using IniParser.Model;

using MessagePack;

using AlgoResult = System.Tuple<object,double>;

namespace Raman
{
    public class ContinuedFraction : IAlgorithm
    {
        Coefficients _aCoeff;
        Coefficients _bCoeff;
        int _depthLimit;

        public event Action<long, long> Progress;
        public event Action<AlgoResult> Result;

        public int Count { get; private set; }

        public AlgorithmType Type { get { return AlgorithmType.ContinuedFraction; } }

        public int Precision { get; private set; }


        public ContinuedFraction(IniData iniFile)
        {
            var config = iniFile[GetType().Name];

            var a_degrees = int.Parse(config["a_degrees"]);
            var b_degrees = int.Parse(config["b_degrees"]);

            _aCoeff = new Coefficients(a_degrees, config["a_coeffs"]);
            _bCoeff = new Coefficients(b_degrees, config["b_coeffs"]);

            _depthLimit  = int.Parse(config["depth_limit"]);

            Count = _aCoeff.Count * _bCoeff.Count;
            Precision = int.Parse(config["precision"]);
        }

        public override string ToString()
        {
            return GetType().Name;
        }

        public void Compute()
        {            
            var total = _aCoeff.Count;
            var aIndex = -1;
            // Parallel.ForEach (_aCoeff.Next(), (aCoeff, state, index) =>
            foreach (var aCoeff in _aCoeff.Next())
            {
                aIndex++;
                var bIndex = -1;

                if (Progress != null) 
                    Progress(aIndex, total);

                foreach (var bCoeff in _bCoeff.Next())
                {
                    bIndex++;

                    var converging = true; // are we converging on a value
                    var depthLimitReached = false; // just a flag to see if we gave up

                    // This is the depth we are starting at. We start with 
                    // a small value in case we are way off so we don't waste
                    // time. This value is increased rapidly and we do another
                    // pass to see if we get closer. (see below)
                    var depth = 10;

                    // This is the accumulated continued fraction value
                    var fraction = 0d;

                    var a0 = aCoeff.EvalPolyAt(0);

                    var result = 0d;
                    var prevResult = double.MaxValue;

                    while (converging && !depthLimitReached)
                    {
                        for (int x = depth; x > 0; x--)
                        {
                            var apoly = aCoeff.EvalPolyAt(x);
                            var bpoly = bCoeff.EvalPolyAt(x);

                            if (apoly + fraction == 0 || bpoly == 0)
                            {
                                converging = false;
                                break;
                            }

                            fraction = bpoly / (apoly + fraction);

                            result = a0 + fraction;
                        }


                        // Check if result and prevResult are the same 
                        // to 'precision' decimal places
                        if (result.IsApproximately(prevResult, Precision))
                        {
                            if (Result != null)
                                Result(new AlgoResult(new {a=aIndex, b=bIndex}, result));

                            if (result.IsApproximately(Math.E))
                            {
                                Console.WriteLine($"{result}");
                            }

                            break;
                        }

                        prevResult = result;

                        depth *= 5;

                        if (depth > _depthLimit)
                        {
                            depthLimitReached = true;
                            break;
                        }
                    }
                }
            }
        }
    }
}