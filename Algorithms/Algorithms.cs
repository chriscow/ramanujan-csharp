using System;
using System.Collections.Generic;
using IniParser.Model;

using AlgoResult = System.Tuple<object,double>;

namespace Raman
{
    public enum AlgorithmType
    {
        ContinuedFraction,
        RationalFunction
    }

    public interface IAlgorithm
    {
        AlgorithmType Type { get; }

        int Count { get; }

        event Action<long, long> Progress;
        event Action<AlgoResult> Result;
        
        void Compute();

        string ToString();
    }

    public static class AlgorithmFactory
    {
        /// <summary>
        /// Parses a string into an Algorithm enum value.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static AlgorithmType ToAlgoType(this string value)
        {
            AlgorithmType result;

            switch (value.ToLower())
            {
                case "continuedfraction":
                    result = AlgorithmType.ContinuedFraction;
                    break;

                case "rationalfunction":
                    result = AlgorithmType.RationalFunction;
                    break;

                default:
                    throw new ArgumentException($"Unknown algorithm: {value}");
            }

            return result;
        }

        /// <summary>
        /// Instantiates the algorithm implementation based on the passed Algorithm enum value.
        /// </summary>
        /// <param name="algoType"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static IAlgorithm Create(AlgorithmType algoType, IniData config)
        {
            IAlgorithm algorithm = null;

            switch (algoType)
            {
                case AlgorithmType.ContinuedFraction:
                    algorithm = new ContinuedFraction(config);
                    break;

                case AlgorithmType.RationalFunction:
                    algorithm = new RationalFunction(config);
                    break;

                default:
                    throw new ArgumentException($"Unimplemented algorithm type: {algoType}");
            }

            return algorithm;
        }
    }
}