using System;
using System.Linq;
using System.Collections.Generic;

using System.Text;

namespace Raman
{
    public static class MathUtil
    {
        /// <summary>
        /// Pass the coefficient in increasing order
        /// i.e.
        /// 
        /// c + bx + ax^2
        /// 
        /// or
        /// 
        /// d + cx + bx^2 + ax^3
        /// 
        /// </summary>
        /// <param name="coefficients"></param>
        /// <returns></returns>
        public static double EvalPolyAt(this IEnumerable<double> coefficients, double x)
        {            
            return coefficients.Select((coefficient, order) =>
            {
                return coefficient * Math.Pow(x, order);
            })
            .Sum();
        }

        public static string FormatContinuedFraction(double result, double[] aCoeff, double[] bCoeff)
        {
            return string.Format(@"
                    {5}
            {1} + ------------
                        {6}
                {2} + ------------
                            {7}              = {0}
                    {3} + -----------
                                {8}
                        {4} + -----------   
                                [...]", 
                                    result, 
                                        aCoeff.EvalPolyAt(0),
                                        aCoeff.EvalPolyAt(1),
                                        aCoeff.EvalPolyAt(2),
                                        aCoeff.EvalPolyAt(3),
                                        bCoeff.EvalPolyAt(1),
                                        bCoeff.EvalPolyAt(2),
                                        bCoeff.EvalPolyAt(3),
                                        bCoeff.EvalPolyAt(4)
                                        );
        }

        public static string ToPolyString(this IEnumerable<double> coefficients, double x)
        {
            var terms = coefficients.ToArray();
            var sb = new string[terms.Length];
            for (int order = 0; order < terms.Length; order++)
            {
                sb[order] = $"{terms[order]}";
                if (order > 0)
                {
                    if (order == 1)
                        sb[order] += $"({x})";
                    else
                        sb[order] += $"({x})^{order}";
                }
            }

            Array.Reverse(sb);
            return string.Join(" + ", sb);
        }

        public static double Truncate(this double value, int places = 5)
        {
            int mul = (int)Math.Pow(10, places);
            return (int)(value * mul) / (double)mul;
        }

        public static bool IsApproximately(this double value1, double value2, int places = 5)
        {
            int mul = (int)Math.Pow(10, places);
            return (int)(value1 * mul) == (int)(value2 * mul);
        }
    }
}